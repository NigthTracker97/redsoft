export const state = () => ({
  products: []
});

export const actions = {
  initProducts({commit}, products) {
    if (!products) {
      const items = [
        {
          id: 1,
          img: require('../assets/img/products/one.png'),
          name: '«Рождение Венеры» Сандро Боттичелли',
          priceOld: '2 000 000 $',
          price: '1 000 000 $',
          isBasket: false
        },
        {
          id: 2,
          img: require('../assets/img/products/two.png'),
          name: '«Тайная вечеря» Леонардо да Винчи',
          price: '3 000 000 $',
          isBasket: false
        },
        {
          id: 3,
          img: require('../assets/img/products/three.png'),
          name: '«Сотворение Адама» Микеланджело',
          priceOld: '6 000 000 $',
          price: '5 000 000 $',
          isBasket: true
        },
        {
          id: 4,
          img: require('../assets/img/products/four.png'),
          name: '«Урок анатомии» Рембрандт',
          priceOld: '2 000 000 $',
          price: '1 000 000 $',
          isBasket: false,
          isBuy: true
        }
      ];
      commit('setInitProducts', items);
    } else {
      commit('setInitProducts', products);
    }
  }
}

export const mutations = {
  setInitProducts(state, products) {
    state.products = products;
  },
  setProducts(state, id) {
    state.products = state.products.map((item) => {
      if (item.id === id) {
        item.isBasket = true;
      }
      return item
    });
    localStorage.setItem('productsItem', JSON.stringify(state.products));
  }
}

export const getters = {
  products: state => state.products,
}
